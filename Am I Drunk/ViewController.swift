//
//  ViewController.swift
//  Am I Drunk
//
//  Created by Rocky on 10/8/18.
//  Copyright © 2018 Rocky. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    @IBAction func addObjectPressed(_ sender: Any) {
        let node = SCNNode()
        node.geometry = SCNBox(width: 0.05, height: 0.01, length: 1, chamferRadius: 0)
        node.geometry?.firstMaterial?.diffuse.contents = UIColor.white
        //TODO: get the Y axis value dynamically from phone's gyrometer to find out the user's current position and place the line on the floor
        node.position = SCNVector3(0.1, -0.7, 0.1)
        self.sceneView.scene.rootNode.addChildNode(node)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        // Create a new scene
        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        
        // Set the scene to the view
        sceneView.scene = scene
//        self.sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints,ARSCNDebugOptions.showWorldOrigin]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        //enable plane detection
        configuration.planeDetection = [.horizontal, .vertical]
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }

    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        if case .limited (let reason) = camera.trackingState {
            //Guide user to improve tracking state either guiding them to not move their phone faster or use the app when in a moving state like a bus or an elevator(May be show this on an alert controller)
        }
    }
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
